Source: gandi-cli
Maintainer: Ben Finney <bignose@debian.org>
Section: utils
Priority: optional
Build-Depends-Indep:
    bash-completion,
    python3-setuptools,
    python3-yaml,
    python3-click (>= 7.0),
    python3-requests,
    python3-ipy,
    python3-all,
    dh-python
Build-Depends:
    debhelper-compat (= 12)
Standards-Version: 4.4.1
Homepage: https://pypi.python.org/pypi/gandi.cli/
VCS-Git: https://salsa.debian.org/debian/pkg-gandi-cli.git
VCS-Browser: https://salsa.debian.org/debian/pkg-gandi-cli/
Rules-Requires-Root: no

Package: gandi-cli
Architecture: all
Depends:
    ${python3:Depends},
    ${misc:Depends}
Description: command-line interface for Gandi service
 The ‘gandi’ command-line interface provides a tool for Gandi
 customers to manage web services:
 .
  * Register a domain name.
  * Create a virtual machine.
  * Deploy a web application with Simple Hosting.
  * Create a SSL Certificate.
  * Add a Web Application vhost with SSL.
  * Create a Private VLAN.
 .
 Gandi is a provider of domain name registration, web hosting, and
 related services.
